#include <fftw3.h>

#include "fftwrappers.h"

arma::Mat<std::complex<double>> fft_w3(const arma::Mat<std::complex<double>> &input,
                                       unsigned                               n_rows)
{
    auto result = input;

    int  rank = 1;
    int  n[] = {static_cast<int>(n_rows)};
    int  howmany = result.n_cols;
    int  idist = n_rows;
    int  odist = n_rows;
    int  istride = 1;
    int  ostride = 1;
    int *inembed = n;
    int *onembed = n;

    if (result.n_rows != 1)
    {
        result.resize(n_rows, result.n_cols);
    }
    else
    {
        result.resize(1, n_rows);
        howmany = 1;
    }

    fftw_complex *an_array = reinterpret_cast<fftw_complex *>(result.memptr());

    /*     auto plan = fftwnd_create_plan(rank, n, FFTW_FORWARD, FFTW_IN_PLACE);

    fftwnd(plan, howmany, an_array, istride, idist, an_array, ostride, odist); */

    auto plan = fftw_plan_many_dft(
        rank, n, howmany, an_array, inembed, istride, idist, an_array, onembed, ostride, odist,
        FFTW_FORWARD, FFTW_CONSERVE_MEMORY | FFTW_PRESERVE_INPUT | FFTW_UNALIGNED | FFTW_ESTIMATE);

    fftw_execute(plan);

    return result;
}
