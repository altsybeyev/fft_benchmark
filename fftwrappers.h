#ifndef FFTWRAPPERS
#define FFTWRAPPERS

#include <armadillo>

arma::Mat<std::complex<double>> fft_w3(const arma::Mat<std::complex<double>> &input,
                                       unsigned                               n_rows);

arma::Mat<std::complex<double>> fft_w2(const arma::Mat<std::complex<double>> &input,
                                       unsigned                               n_rows);

#endif