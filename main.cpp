#include <chrono>
#include <iostream>

#include <armadillo>

#include "fftwrappers.h"

int main()
{
    std::chrono::time_point<std::chrono::system_clock> start, end;

    std::cout << "Matrix generation" << std::endl;
    size_t n_rows = 479;
    size_t n_rows_new = n_rows * 10;

    size_t n_cols = 400;

    arma::mat real;
    arma::mat imag;

    real.randu(n_rows, n_cols);
    imag.randu(n_rows, n_cols);

    arma::cx_mat A(real, imag);

    start = std::chrono::system_clock::now();
    std::cout << "fftw2 computing..." << std::endl;
    auto fftw2_res = fft_w2(A, n_rows_new);
    std::cout << "fftw2 time = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(
                     std::chrono::system_clock::now() - start)
                     .count()
              << std::endl;

    start = std::chrono::system_clock::now();
    std::cout << "fftw3 computing..." << std::endl;
    auto fftw3_res = fft_w3(A, n_rows_new);
    std::cout << "fftw3 time = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(
                     std::chrono::system_clock::now() - start)
                     .count()
              << std::endl;

    start = std::chrono::system_clock::now();
    std::cout << "arma computing..." << std::endl;
    arma::cx_mat arma_res = arma::fft(A, n_rows_new);
    std::cout << "arma time = "
              << std::chrono::duration_cast<std::chrono::milliseconds>(
                     std::chrono::system_clock::now() - start)
                     .count()
              << std::endl;

    std::cout << "delta fftw2-arma = " << (fftw2_res - arma_res).max() << std::endl;
    std::cout << "delta fftw3-arma = " << (fftw3_res - arma_res).max() << std::endl;

    return 0;
}